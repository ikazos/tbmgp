# `tbmgp`

A transition-based Minimalist Grammar parser with backtracking, based on [Stanojević (2016)](https://link.springer.com/chapter/10.1007/978-3-662-53826-5_17).

The parser as described by Stanojević (2016) requires an oracle that chooses a transition at each state of the parser according to some heuristic.  Right now, there are two such heuristics:

*   `uniform`

    The parser determines the next transition by randomly sampling a transition from a uniform distribution over all available transitions.

*   `interactive`

    The parser presents the user with the available next transitions and asks the user to choose one.

## Install

This project requires Python 3 (the higher the minor version the better) and numpy.  That should be all you need to install to run this.  Then, try:

```
> cd src
> python -m tbmgp.parse ../samplemg.txt ../input1.txt --heuristic uniform -k 1
```

This should parse the sentence *Phong draws Roki* in about 1 second.  For more info on how to invoke the parser, see the documentation.

## Documentation

The documentation is hosted at [https://ikazos.gitlab.io/tbmgp/].  You can use `pdoc` to build documentation:

```
> cd src
> pdoc tbmgp -o ../docs --math
```
