"""
Parser.  This file can be run as a script.

# What's in this module

# Running the parser as a script

Run this script with:

```
> python parse.py MG INPUTS --heuristic HEURISTIC -k K
```
"""



from typing import Optional, Union
from .mg import *

from collections import namedtuple
from dataclasses import dataclass
import argparse
import numpy as np



LEXICAL = 1
"""Lexical type of a chain."""

DERIVED = 2
"""Derived type of a chain."""

NUM_TRANSITIONS = 6
"""Number of possible transitions."""

SELECT = 0
"""The *select* transition."""

SELECT_EPSILON = 1
"""The *selectEpsilon* transition."""

TMERGE = 2
"""The *tmerge* transition."""

TMOVE = 3
"""The *tmove* transition."""

SWAP = 4
"""The *swap* transition."""

TAKE_BACK = 5
"""The *takeBack* transition."""

SELECT_LOWERCASE_NAME = "select"
SELECT_EPSILON_LOWERCASE_NAME = "selectepsilon"
TMERGE_LOWERCASE_NAME = "tmerge"
TMOVE_LOWERCASE_NAME = "tmove"
SWAP_LOWERCASE_NAME = "swap"
TAKE_BACK_LOWERCASE_NAME = "takeback"

TRANSITION_INPUT_MAP = {
    SELECT_LOWERCASE_NAME: SELECT,
    SELECT_EPSILON_LOWERCASE_NAME: SELECT_EPSILON,
    TMERGE_LOWERCASE_NAME: TMERGE,
    TMOVE_LOWERCASE_NAME: TMOVE,
    SWAP_LOWERCASE_NAME: SWAP,
    TAKE_BACK_LOWERCASE_NAME: TAKE_BACK,
}



IndexPair = namedtuple("IndexPair", [ "begin", "end" ])
"""
A pair of indices (`begin` and `end`), which represents a span over the input string.
"""


Wildcard = namedtuple("Wildcard", [])
"""
A wildcard, i.e., a pair of indices that represents some empty span over the input string.
"""



Chain = namedtuple("Chain", [ "indices", "type", "sfseq" ])
"""
Chain.  Consists of:

*   A pair of indices (`indices`);
*   A type (`type`), which can be either `LEXICAL` or `DERIVED`;
*   A syntactic feature sequence (`sfseq`).
"""



MinimalistItem = namedtuple("MinimalistItem", [ "chains" ])
"""
Minimalist item, i.e., a list of chains (`chains`).
"""



Configuration = namedtuple("Configuration", [ "main_stack", "aux_stack", "buffer", "num_epsilons" ])
"""
Parser configuration.  Consists of four control structures:

*   The **main stack** (`main_stack`), which stores minimalist items built by the MG syntactic operations;
*   The **auxiliary stack** (`aux_stack`), which is used for reordering minimalist items in the main stack;
*   The **buffer** (`buffer`), i.e. the sequence of lexical items waiting to be selected;
*   The **epsilon transition count** (`num_epsilons`), which tracks how many empty strings have been generated in the current derivation.
"""



@dataclass
class TransitionMask:
    """
    Mask that represents whether each transition is allowed from some configuration.

    *   `can_select_lexical_entry_idxs` is a list.  This list is empty if *select* is not allowed.  Otherwise, this list contains the indices of the lexical entries that can be selected.
    *   `can_select_epsilon_lexical_entry_idxs` is a list.  This list is empty if *selectEpsilon* is not allowed.  Otherwise, this list contains the indices of the lexical entries with an empty symbol that can be selected.
    *   The remaining four fields `can_tmerge`, `can_tmove`, `can_swap` and `can_take_back` are bools.  Each field is True iff the corresponding transition is allowed.
    """

    can_select_lexical_entry_idxs: List[int]
    can_select_epsilon_lexical_entry_idxs: List[int]
    can_tmerge: bool
    can_tmove: bool
    can_swap: bool
    can_take_back: bool



Transition = namedtuple("Transition", [ "type", "lexical_entry_idx" ])
"""
Transition.  Consists of:

*   A type (`type`), which can be one of `SELECT`, `SELECT_EPSILON`, `TMERGE`, `TMOVE`, `SWAP`, `TAKE_BACK`;
*   An index to a selected lexical entry (`lexical_entry_idx`).  This should be None if `type` is neither `SELECT` nor `SELECT_EPSILON`.  Otherwise, this is an integer that indexes the lexical entry to be selected.
"""



def fmt_indices(indices: IndexPair | Wildcard) -> str:
    """
    Format a pair of indices.

    :param indices: Either a pair of indices, or a wildcard.
    :return: The formatted string.
    """

    if isinstance(indices, IndexPair):
        return f"({indices.begin}, {indices.end})"
    else:
        return f"(*, *)"



def fmt_chain(chain: Chain) -> str:
    """
    Format a chain.

    :param chain: The chain to be formatted.
    :return: The formatted string.
    """

    fmt_type = "::" if chain.type == LEXICAL else ":"
    return f"{fmt_indices(chain.indices)} {fmt_type} " + " ".join(map(fmt_syntactic_feature, chain.sfseq))



def fmt_minimalist_item(item: MinimalistItem) -> str:
    """
    Format a minimalist item.

    :param item: The minimalist item to be formatted.
    :return: The formatted string.
    """

    return "{" + ", ".join(map(fmt_chain, item.chains)) + "}"



def fmt_config(config: Configuration) -> str:
    """
    Format a configuration.

    :param configuration: The configuration to be formatted.
    :return: The formatted string.
    """

    fmt_main_stack = "[]" if len(config.main_stack) == 0 else "[ " + ",\n             ".join(map(fmt_minimalist_item, config.main_stack)) + " ]"
    fmt_aux_stack = "[]" if len(config.aux_stack) == 0 else "[ " + ",\n             ".join(map(fmt_minimalist_item, config.aux_stack)) + " ]"

    s = ""
    s += f"< main   = {fmt_main_stack},\n"
    s += f"  aux    = {fmt_aux_stack},\n"
    s += f"  buffer = {config.buffer},\n"
    s += f"  k      = {config.num_epsilons} >"

    return s



def axiom(n: int) -> Configuration:
    """
    Create an axiom configuration.

    :param n: The number of lexical items.
    :return: The configuration.
    """

    return Configuration([], [], list(range(n)), 0)



def is_goal(config: Configuration, n: int, max_num_epsilons: int, mg: MinimalistGrammar) -> bool:
    """
    Check if a given configuration is a goal configuration.

    :param config: The configuration to check.
    :param n: The number of lexical items.
    :param max_num_epsilons: The maximum number of empty strings allowed.
    :return: `True` iff `config` is a goal configuration.
    """

    item = MinimalistItem([
        Chain(IndexPair(0, n), DERIVED, [ selectee(mg.complete_feature) ])
    ])

    return (
        (len(config.main_stack) == 1) and
        (config.main_stack[0] == item) and
        (len(config.aux_stack) == 0) and
        (len(config.buffer) == 0) and
        (config.num_epsilons <= max_num_epsilons)
    )



def select(config: Configuration, lexical_entry_idx: int, mg: MinimalistGrammar) -> Configuration:
    """
    Perform the *select* transition.
    
    :param config: The configuration to perform the transition on.
    :param lexical_entry_idx: The index of the lexical entry to be selected in the lexicon of `mg`.
    :param mg: The grammar.
    :return: The configuration after the transition.
    """
    
    i = config.buffer[0]
    sfseq = mg.lexical_entries[lexical_entry_idx].sfseq

    return Configuration(
        config.main_stack + [ MinimalistItem([ Chain(IndexPair(i, i+1), LEXICAL, sfseq) ]) ],
        config.aux_stack,
        config.buffer[1:],
        config.num_epsilons
    )



def select_epsilon(config: Configuration, lexical_entry_idx: int, mg: MinimalistGrammar) -> Configuration:
    """
    Perform the *selectEpsilon* transition.
    
    :param config: The configuration to perform the transition on.
    :param lexical_entry_idx: The index of the lexical entry to be selected in the lexicon of `mg`.
    :param mg: The grammar.
    :return: The configuration after the transition.
    """

    sfseq = mg.lexical_entries[lexical_entry_idx].sfseq

    return Configuration(
        config.main_stack + [ MinimalistItem([ Chain(Wildcard(), LEXICAL, sfseq) ]) ],
        config.aux_stack,
        config.buffer,
        config.num_epsilons + 1
    )



def can_merge1(x: MinimalistItem, y: MinimalistItem) -> bool:
    if len(x.chains) != 1:
        return False
    
    if len(y.chains) == 0:
        return False
    
    if x.chains[0].type != LEXICAL:
        return False

    if (
        isinstance(x.chains[0].indices, IndexPair) and
        isinstance(y.chains[0].indices, IndexPair) and
        (x.chains[0].indices.end != y.chains[0].indices.begin)
    ):
        return False
    
    if (
        (len(x.chains[0].sfseq) < 2) or
        (len(y.chains[0].sfseq) != 1) or
        (x.chains[0].sfseq[0].type != SELECTOR) or
        (y.chains[0].sfseq[0].type != SELECTEE) or
        (x.chains[0].sfseq[0].feature != y.chains[0].sfseq[0].feature)
    ):
        return False

    return True



def can_merge2(x: MinimalistItem, y: MinimalistItem) -> bool:
    if len(x.chains) == 0:
        return False
    
    if len(y.chains) == 0:
        return False
    
    if x.chains[0].type != DERIVED:
        return False

    if (
        isinstance(x.chains[0].indices, IndexPair) and
        isinstance(y.chains[0].indices, IndexPair) and
        (x.chains[0].indices.begin != y.chains[0].indices.end)
    ):
        return False
    
    if (
        (len(x.chains[0].sfseq) < 2) or
        (len(y.chains[0].sfseq) != 1) or
        (x.chains[0].sfseq[0].type != SELECTOR) or
        (y.chains[0].sfseq[0].type != SELECTEE) or
        (x.chains[0].sfseq[0].feature != y.chains[0].sfseq[0].feature)
    ):
        return False

    return True



def can_merge3(x: MinimalistItem, y: MinimalistItem) -> bool:
    if len(x.chains) == 0:
        return False
    
    if len(y.chains) == 0:
        return False
    
    if (
        (len(x.chains[0].sfseq) < 2) or
        (len(y.chains[0].sfseq) < 2) or
        (x.chains[0].sfseq[0].type != SELECTOR) or
        (y.chains[0].sfseq[0].type != SELECTEE) or
        (x.chains[0].sfseq[0].feature != y.chains[0].sfseq[0].feature)
    ):
        return False

    return True



def can_merge(x: MinimalistItem, y: MinimalistItem) -> bool:
    return can_merge1(x, y) or can_merge2(x, y) or can_merge3(x, y)



def merge1(x: MinimalistItem, y: MinimalistItem) -> MinimalistItem:
    if isinstance(x.chains[0].indices, IndexPair):
        if isinstance(y.chains[0].indices, IndexPair):
            new_indices = IndexPair(x.chains[0].indices.begin, y.chains[0].indices.end)
        else:
            new_indices = x.chains[0].indices
    else:
        new_indices = y.chains[0].indices

    new_chain = Chain(new_indices, DERIVED, x.chains[0].sfseq[1:])

    return MinimalistItem([ new_chain ] + y.chains[1:])



def merge2(x: MinimalistItem, y: MinimalistItem) -> MinimalistItem:
    if isinstance(x.chains[0].indices, IndexPair):
        if isinstance(y.chains[0].indices, IndexPair):
            new_indices = IndexPair(y.chains[0].indices.begin, x.chains[0].indices.end)
        else:
            new_indices = x.chains[0].indices
    else:
        new_indices = y.chains[0].indices

    new_chain = Chain(new_indices, DERIVED, x.chains[0].sfseq[1:])

    return MinimalistItem([ new_chain ] + x.chains[1:] + y.chains[1:])



def merge3(x: MinimalistItem, y: MinimalistItem) -> MinimalistItem:
    new_head = Chain(x.chains[0].indices, DERIVED, x.chains[0].sfseq[1:])
    new_nonhead = Chain(y.chains[0].indices, DERIVED, y.chains[0].sfseq[1:])

    return MinimalistItem([ new_head ] + x.chains[1:] + [ new_nonhead ] + y.chains[1:])



def tmerge(config: Configuration) -> Configuration:
    """
    Perform the *tmerge* transition.
    
    :param config: The configuration to perform the transition on.
    :return: The configuration after the transition.
    """

    x = config.main_stack[-2]
    y = config.main_stack[-1]

    # Which merge is it?
    if len(y.chains[0].sfseq) > 1:
        z = merge3(x, y)
    else:
        if x.chains[0].type == LEXICAL:
            z = merge1(x, y)
        else:
            z = merge2(x, y)

    return Configuration(
        config.main_stack[:-2] + [ z ],
        config.aux_stack,
        config.buffer,
        config.num_epsilons
    )



def can_move1(x: MinimalistItem) -> Optional[int]:
    if len(x.chains) < 2:
        return False

    if x.chains[0].type != DERIVED:
        return False
    
    if (
        (len(x.chains[0].sfseq) < 2) or
        (x.chains[0].sfseq[0].type != LICENSOR)
    ):
        return False
    
    found_i = None
    for i, chain in enumerate(x.chains):
        if i == 0:
            continue

        if (
            isinstance(x.chains[0].indices, IndexPair) and
            isinstance(chain.indices, IndexPair) and
            (x.chains[0].indices.begin != chain.indices.end)
        ):
            continue
    
        if (
            (len(chain.sfseq) != 1) or
            (chain.sfseq[0].type != LICENSEE) or
            (x.chains[0].sfseq[0].feature != chain.sfseq[0].feature)
        ):
            continue

        if found_i is not None:
            return None
        
        found_i = i

    return found_i



def can_move2(x: MinimalistItem) -> Optional[int]:
    if len(x.chains) < 2:
        return False

    if x.chains[0].type != DERIVED:
        return False
    
    if (
        (len(x.chains[0].sfseq) < 2) or
        (x.chains[0].sfseq[0].type != LICENSOR)
    ):
        return False
    
    found_i = None
    for i, chain in enumerate(x.chains):
        if i == 0:
            continue

        if (
            (len(chain.sfseq) < 2) or
            (chain.sfseq[0].type != LICENSEE) or
            (x.chains[0].sfseq[0].feature != chain.sfseq[0].feature)
        ):
            continue

        if found_i is not None:
            return None
        
        found_i = i

    return found_i



def can_move(x: MinimalistItem) -> Optional[int]:
    move1_goal_i = can_move1(x)
    move2_goal_i = can_move2(x)

    if move1_goal_i is not None:
        # If neither of the above two indices are None, then we have a violation of SMC
        if move2_goal_i is not None:
            return None
        else:
            return move1_goal_i
    else:
        return move2_goal_i



def move1(x: MinimalistItem) -> MinimalistItem:
    goal_i = can_move(x)
    goal = x.chains[goal_i]

    if isinstance(x.chains[0].indices, IndexPair):
        if isinstance(goal.indices, IndexPair):
            new_indices = IndexPair(goal.indices.begin, x.chains[0].indices.end)
        else:
            new_indices = x.chains[0].indices
    else:
        new_indices = goal.indices

    new_chain = Chain(new_indices, DERIVED, x.chains[0].sfseq[1:])

    return MinimalistItem([ new_chain ] + x.chains[1:goal_i] + x.chains[goal_i+1:])



def move2(x: MinimalistItem) -> MinimalistItem:
    goal_i = can_move(x)
    goal = x.chains[goal_i]

    new_probe = Chain(x.chains[0].indices, DERIVED, x.chains[0].sfseq[1:])
    new_goal = Chain(goal.indices, DERIVED, goal.sfseq[1:])

    return MinimalistItem([ new_probe ] + x.chains[1:goal_i] + [ new_goal ] + x.chains[goal_i+1:])



def tmove(config: Configuration) -> Configuration:
    """
    Perform the *tmove* transition.
    
    :param config: The configuration to perform the transition on.
    :return: The configuration after the transition.
    """

    x = config.main_stack[-1]

    # Which move is it?
    goal = x.chains[can_move(x)]
    if len(goal.sfseq) > 1:
        y = move2(x)
    else:
        y = move1(x)

    return Configuration(
        config.main_stack[:-1] + [ y ],
        config.aux_stack,
        config.buffer,
        config.num_epsilons
    )



def swap(config: Configuration) -> Configuration:
    """
    Perform the *swap* transition.
    
    :param config: The configuration to perform the transition on.
    :return: The configuration after the transition.
    """

    x = config.main_stack[-2]
    y = config.main_stack[-1]

    return Configuration(
        config.main_stack[:-2] + [ y ],
        [ x ] + config.aux_stack,
        config.buffer,
        config.num_epsilons
    )



def take_back(config: Configuration) -> Configuration:
    """
    Perform the *takeBack* transition.
    
    :param config: The configuration to perform the transition on.
    :return: The configuration after the transition.
    """

    x = config.aux_stack[0]

    return Configuration(
        config.main_stack + [ x ],
        config.aux_stack[1:],
        config.buffer,
        config.num_epsilons
    )



def parse_uniform(mg: MinimalistGrammar, inputs: List[str], max_num_epsilons: int):
    """
    Parse with the uniform heuristic.
    
    :param mg: The grammar.
    :param inputs: List of input strings.
    :param max_num_epsilons: Maximum number of empty symbols.
    """

    epsilon_lexical_entry_idxs = []
    for i, lexical_entry in enumerate(mg.lexical_entries):
        if (lexical_entry.symbol == ""):
            epsilon_lexical_entry_idxs.append(i)

    # Parse!
    for line in inputs:
        line = line.split(" ")

        # Are all lexical items in the grammar?
        lexical_entry_idxss = []
        for lexical_item in line:
            lexical_entry_idxs = []

            for i, lexical_entry in enumerate(mg.lexical_entries):
                if (lexical_entry.symbol == lexical_item):
                    lexical_entry_idxs.append(i)

            if len(lexical_entry_idxs) == 0:
                raise Exception(f"Invalid lexical item \"{lexical_item}\"")

            lexical_entry_idxss.append(lexical_entry_idxs)

        config = axiom(len(line))

        # The search path is a path on a search tree.  As a result, the search
        # path is a list of nodes.  Each node is a pair that consists of a
        # configuration and a transition mask -- the transition mask represents
        # the set of possible transitions that we haven't tried yet.
        search_path = [ [config, None] ]

        rng = np.random.default_rng()

        while len(search_path) > 0:
            # Get the deepest node.
            config, transition_mask = search_path[-1]

            print(fmt_config(config))

            # Are we done?
            if is_goal(config, len(line), max_num_epsilons, mg):
                print("Parsed!")
                break

            # If we visit this node for the first time...
            if transition_mask is None:
                # We should figure out what transitions from this configuration
                # are possible.
                can_select = len(config.buffer) >= 1
                can_select_lexical_entry_idxs = lexical_entry_idxss[config.buffer[0]].copy() if can_select else []
                
                can_select_epsilon = config.num_epsilons < args.max_num_epsilons
                can_select_epsilon_lexical_entry_idxs = epsilon_lexical_entry_idxs.copy() if can_select_epsilon else []

                can_tmerge = (
                    (len(config.main_stack) >= 2) and
                    can_merge(config.main_stack[-2], config.main_stack[-1])
                )
                can_tmove = (
                    (len(config.main_stack) >= 1) and
                    can_move(config.main_stack[-1]) is not None
                )
                can_swap = (
                    (len(config.main_stack) >= 2) and
                    (
                        isinstance(config.main_stack[-1].chains[0].indices, Wildcard) or
                        (
                            isinstance(config.main_stack[-2].chains[0].indices, IndexPair) and
                            config.main_stack[-2].chains[0].indices.begin < config.main_stack[-1].chains[0].indices.begin
                        )
                    )
                )
                can_take_back = len(config.aux_stack) >= 1

                transition_mask = TransitionMask(
                    can_select_lexical_entry_idxs,
                    can_select_epsilon_lexical_entry_idxs,
                    can_tmerge,
                    can_tmove,
                    can_swap,
                    can_take_back
                )
                search_path[-1][1] = transition_mask

            # Figure out which transitions to explore.  For a transition to be
            # worthy of exploration, it needs to be possible in the first place.
            # This means we should find it in the transition mask.
            p = np.array([
                transition_mask.can_select_lexical_entry_idxs != [],
                transition_mask.can_select_epsilon_lexical_entry_idxs != [],
                transition_mask.can_tmerge,
                transition_mask.can_tmove,
                transition_mask.can_swap,
                transition_mask.can_take_back,
            ])

            print(
                "Available transitions: " +
                ", ".join(filter(lambda x: x is not None, [
                    f"select({transition_mask.can_select_lexical_entry_idxs})"                if p[0] else None,
                    f"selectEpsilon({transition_mask.can_select_epsilon_lexical_entry_idxs})" if p[1] else None,
                    "tmerge"                                                                  if p[2] else None,
                    "tmove"                                                                   if p[3] else None,
                    "swap"                                                                    if p[4] else None,
                    "takeBack"                                                                if p[5] else None,
                ]))
            )

            # How many transitions are worthy of exploration?
            sum = p.sum()

            # If there are none, then we are stuck; backtrack.
            if sum == 0:
                print("Backtrack...")
                search_path.pop()
                continue

            # Otherwise, pick a transition...
            p = p / sum
            type = rng.choice(NUM_TRANSITIONS, p=p)
            lexical_entry_idx = None
            if type == SELECT:
                lexical_entry_idx = rng.choice(transition_mask.can_select_lexical_entry_idxs)
            elif type == SELECT_EPSILON:
                lexical_entry_idx = rng.choice(transition_mask.can_select_epsilon_lexical_entry_idxs)

            transition = Transition(type, lexical_entry_idx)

            # ... apply it to the current configuration, remove the transition
            # from the transition mask...
            if transition.type == SELECT:
                print(f"Next transition: select({transition.lexical_entry_idx})")
                new_config = select(config, transition.lexical_entry_idx, mg)
                transition_mask.can_select_lexical_entry_idxs.remove(transition.lexical_entry_idx)

            elif transition.type == SELECT_EPSILON:
                print(f"Next transition: selectEpsilon({transition.lexical_entry_idx})")
                new_config = select_epsilon(config, transition.lexical_entry_idx, mg)
                transition_mask.can_select_epsilon_lexical_entry_idxs.remove(transition.lexical_entry_idx)

            elif transition.type == TMERGE:
                print("Next transition: tmerge")
                new_config = tmerge(config)
                transition_mask.can_tmerge = False

            elif transition.type == TMOVE:
                print("Next transition: tmove")
                new_config = tmove(config)
                transition_mask.can_tmove = False

            elif transition.type == SWAP:
                print("Next transition: swap")
                new_config = swap(config)
                transition_mask.can_swap = False

            elif transition.type == TAKE_BACK:
                print("Next transition: takeBack")
                new_config = take_back(config)
                transition_mask.can_take_back = False

            else:
                raise Exception("unknown transition type")

            # ... and grow the search path.
            search_path.append([ new_config, None ])

        if len(search_path) == 0:
            print("Failed to parse this sentence.")



def parse_interactive(mg: MinimalistGrammar, inputs: List[str], max_num_epsilons: int):
    """
    Parse with the interactive heuristic.
    
    :param mg: The grammar.
    :param inputs: List of input strings.
    :param max_num_epsilons: Maximum number of empty symbols.
    """

    epsilon_lexical_entry_idxs = []
    for i, lexical_entry in enumerate(mg.lexical_entries):
        if (lexical_entry.symbol == ""):
            epsilon_lexical_entry_idxs.append(i)

    # Parse!
    for line in inputs:
        line = line.split(" ")

        # Are all lexical items in the grammar?
        lexical_entry_idxss = []
        for lexical_item in line:
            lexical_entry_idxs = []

            for i, lexical_entry in enumerate(mg.lexical_entries):
                if (lexical_entry.symbol == lexical_item):
                    lexical_entry_idxs.append(i)

            if len(lexical_entry_idxs) == 0:
                raise Exception(f"Invalid lexical item \"{lexical_item}\"")

            lexical_entry_idxss.append(lexical_entry_idxs)

        config = axiom(len(line))

        # The search path is a path on a search tree.  As a result, the search
        # path is a list of nodes.  Each node is a pair that consists of a
        # configuration and a transition mask -- the transition mask represents
        # the set of possible transitions that we haven't tried yet.
        search_path = [ [config, None] ]

        while len(search_path) > 0:
            # Get the deepest node.
            config, transition_mask = search_path[-1]

            print(fmt_config(config))

            # Are we done?
            if is_goal(config, len(line), max_num_epsilons, mg):
                print("Parsed!")
                break

            # If we visit this node for the first time...
            if transition_mask is None:
                # We should figure out what transitions from this configuration
                # are possible.
                can_select = len(config.buffer) >= 1
                can_select_lexical_entry_idxs = lexical_entry_idxss[config.buffer[0]].copy() if can_select else []
                
                can_select_epsilon = config.num_epsilons < args.max_num_epsilons
                can_select_epsilon_lexical_entry_idxs = epsilon_lexical_entry_idxs.copy() if can_select_epsilon else []

                can_tmerge = (
                    (len(config.main_stack) >= 2) and
                    can_merge(config.main_stack[-2], config.main_stack[-1])
                )
                can_tmove = (
                    (len(config.main_stack) >= 1) and
                    can_move(config.main_stack[-1]) is not None
                )
                can_swap = (
                    (len(config.main_stack) >= 2) and
                    (
                        isinstance(config.main_stack[-1].chains[0].indices, Wildcard) or
                        (
                            isinstance(config.main_stack[-2].chains[0].indices, IndexPair) and
                            config.main_stack[-2].chains[0].indices.begin < config.main_stack[-1].chains[0].indices.begin
                        )
                    )
                )
                can_take_back = len(config.aux_stack) >= 1

                transition_mask = TransitionMask(
                    can_select_lexical_entry_idxs,
                    can_select_epsilon_lexical_entry_idxs,
                    can_tmerge,
                    can_tmove,
                    can_swap,
                    can_take_back
                )
                search_path[-1][1] = transition_mask

            # Figure out which transitions to explore.  For a transition to be
            # worthy of exploration, it needs to be possible in the first place.
            # This means we should find it in the transition mask.
            p = np.array([
                transition_mask.can_select_lexical_entry_idxs != [],
                transition_mask.can_select_epsilon_lexical_entry_idxs != [],
                transition_mask.can_tmerge,
                transition_mask.can_tmove,
                transition_mask.can_swap,
                transition_mask.can_take_back,
            ])

            print(
                "Available transitions: " +
                ", ".join(filter(lambda x: x is not None, [
                    f"select({transition_mask.can_select_lexical_entry_idxs})"                if p[0] else None,
                    f"selectEpsilon({transition_mask.can_select_epsilon_lexical_entry_idxs})" if p[1] else None,
                    "tmerge"                                                                  if p[2] else None,
                    "tmove"                                                                   if p[3] else None,
                    "swap"                                                                    if p[4] else None,
                    "takeBack"                                                                if p[5] else None,
                ]))
            )

            # Print relevant lexical entries if a select or selectEpsilon transition is available.
            if len(transition_mask.can_select_lexical_entry_idxs) > 0:
                print("Lexical entries for select:")
                for i in transition_mask.can_select_lexical_entry_idxs:
                    print(f"Index: {i}\tLexical entry: {fmt_lexical_entry(mg.lexical_entries[i])}")

            if len(transition_mask.can_select_epsilon_lexical_entry_idxs) > 0:
                print("Lexical entries for selectEpsilon:")
                for i in transition_mask.can_select_epsilon_lexical_entry_idxs:
                    print(f"Index: {i}\tLexical entry: {fmt_lexical_entry(mg.lexical_entries[i])}")

            # How many transitions are worthy of exploration?
            sum = p.sum()

            # If there are none, then we are stuck; backtrack.
            if sum == 0:
                print("Backtrack.  Enter anything to continue parsing...")
                input("> ")
                search_path.pop()
                continue

            # Otherwise, ask to user to pick a transition...
            type = None
            while (
                (type not in list(TRANSITION_INPUT_MAP.keys())) or
                (type == SELECT_LOWERCASE_NAME and not(p[0])) or
                (type == SELECT_EPSILON_LOWERCASE_NAME and not(p[1])) or
                (type == TMERGE_LOWERCASE_NAME and not(p[2])) or
                (type == TMOVE_LOWERCASE_NAME and not(p[3])) or
                (type == SWAP_LOWERCASE_NAME and not(p[4])) or
                (type == TAKE_BACK_LOWERCASE_NAME and not(p[5]))
            ):
                print("Enter a transition type, one of: select, selectEpsilon, tmerge, tmove, swap, takeBack (case insensitive)...")
                type = input("> ").lower()

            lexical_entry_idx = None
            if type in (SELECT_LOWERCASE_NAME, SELECT_EPSILON_LOWERCASE_NAME):
                lexical_entry_idx = "None"
                while (
                    not(lexical_entry_idx.isdigit()) or
                    (type == "select" and int(lexical_entry_idx) not in transition_mask.can_select_lexical_entry_idxs) or
                    (type == "selectepsilon" and int(lexical_entry_idx) not in transition_mask.can_select_epsilon_lexical_entry_idxs)
                ):
                    print("Enter the index of the lexical entry to select(Epsilon)...")
                    lexical_entry_idx = input("> ")

            transition = Transition(TRANSITION_INPUT_MAP[type], int(lexical_entry_idx) if lexical_entry_idx is not None else None)

            # ... apply it to the current configuration, remove the transition
            # from the transition mask...
            if transition.type == SELECT:
                print(f"Next transition: select({transition.lexical_entry_idx})")
                new_config = select(config, transition.lexical_entry_idx, mg)
                transition_mask.can_select_lexical_entry_idxs.remove(transition.lexical_entry_idx)

            elif transition.type == SELECT_EPSILON:
                print(f"Next transition: selectEpsilon({transition.lexical_entry_idx})")
                new_config = select_epsilon(config, transition.lexical_entry_idx, mg)
                transition_mask.can_select_epsilon_lexical_entry_idxs.remove(transition.lexical_entry_idx)

            elif transition.type == TMERGE:
                print("Next transition: tmerge")
                new_config = tmerge(config)
                transition_mask.can_tmerge = False

            elif transition.type == TMOVE:
                print("Next transition: tmove")
                new_config = tmove(config)
                transition_mask.can_tmove = False

            elif transition.type == SWAP:
                print("Next transition: swap")
                new_config = swap(config)
                transition_mask.can_swap = False

            elif transition.type == TAKE_BACK:
                print("Next transition: takeBack")
                new_config = take_back(config)
                transition_mask.can_take_back = False

            else:
                raise Exception("unknown transition type")

            # ... and grow the search path.
            search_path.append([ new_config, None ])

        if len(search_path) == 0:
            print("Failed to parse this sentence.")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="tbmgp",
        description="Parse a list of sentences using some MG."
    )

    parser.add_argument("grammar_path")
    parser.add_argument("input_path")
    parser.add_argument("--heuristic", type=str, required=True)
    parser.add_argument("-k", "--max-num-epsilons", type=int, default=5)

    args = parser.parse_args()



    # Load grammar
    with open(args.grammar_path, "r") as fi:
        mg = deserialize_mg(fi.read())



    # Load input
    inputs = []
    with open(args.input_path, "r") as fi:
        for line in fi:
            inputs.append(line.strip())



    # Load heuristic
    if args.heuristic == "uniform":
        parse_uniform(mg, inputs, args.max_num_epsilons)
    elif args.heuristic == "interactive":
        parse_interactive(mg, inputs, args.max_num_epsilons)
    else:
        raise Exception("unknown heuristic")
