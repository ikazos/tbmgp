"""
Minimalist grammars.
"""



from collections import namedtuple

import json
from typing import List



SELECTOR = 3
SELECTEE = 4
LICENSOR = 5
LICENSEE = 6



SyntacticFeature = namedtuple("SyntacticFeature", [ "type", "feature" ])
"""
A syntactic feature.  Consists of:

*   A type (`type`), which can be one of four things: `SELECTOR`, `SELECTEE`, `LICENSOR`, `LICENSEE`.
*   A feature (`feature`), which is a string.
"""



LexicalEntry = namedtuple("LexicalEntry", [ "symbol", "sfseq" ])



MinimalistGrammar = namedtuple("MinimalistGrammar", [
    "selecting_features",
    "licensing_features",
    "complete_feature",
    "lexical_entries"
])



def selector(f):
    return SyntacticFeature(SELECTOR, f)

def selectee(f):
    return SyntacticFeature(SELECTEE, f)

def licensor(f):
    return SyntacticFeature(LICENSOR, f)

def licensee(f):
    return SyntacticFeature(LICENSEE, f)



def fmt_syntactic_feature(sf: SyntacticFeature):
    if sf.type == SELECTOR:
        fmt_type = "="
    elif sf.type == SELECTEE:
        fmt_type = ""
    elif sf.type == LICENSOR:
        fmt_type = "+"
    elif sf.type == LICENSEE:
        fmt_type = "-"
    else:
        raise Exception("Invalid type of syntactic feature")
    
    return f"{fmt_type}{sf.feature}"



def fmt_lexical_entry(lexical_entry: LexicalEntry):
    fmt_sfseq = " ".join(map(fmt_syntactic_feature, lexical_entry.sfseq))

    if len(lexical_entry.symbol) == 0:
        return f":: {fmt_sfseq}"
    else:
        return f"{lexical_entry.symbol} :: {fmt_sfseq}"



def get_sample_mg() -> MinimalistGrammar:
    return MinimalistGrammar(
        [ "d", "v", "c" ],
        [ "wh" ],
        "c",
        [
            LexicalEntry("", [ selector("v"), selectee("c") ]),
            LexicalEntry("", [ selector("v"), licensor("wh"), selectee("c") ]),
            LexicalEntry("likes", [ selector("c"), selector("d"), selectee("v") ]),
            LexicalEntry("draws", [ selector("d"), selector("d"), selectee("v") ]),
            LexicalEntry("Phong", [ selectee("d") ]),
            LexicalEntry("Roki", [ selectee("d") ]),
            LexicalEntry("what", [ selectee("d"), licensee("wh") ]),
        ]
    )



def deserialize_mg(s: str) -> MinimalistGrammar:
    def retype_sfseq(args) -> List[SyntacticFeature]:
        return list(map(lambda sf_args: SyntacticFeature(*sf_args), args))
    
    def retype_lexical_entry(symbol: str, sfseq_args) -> LexicalEntry:
        return LexicalEntry(symbol, retype_sfseq(sfseq_args))

    mg_args = json.loads(s)
    mg_args[3] = list(map(lambda le_args: retype_lexical_entry(*le_args), mg_args[3]))
    return MinimalistGrammar(*mg_args)
